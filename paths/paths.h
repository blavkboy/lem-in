/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   paths.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:39:33 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/05 11:12:06 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PATHS_H
# define PATHS_H

# include <libft.h>

typedef struct		s_path
{
	struct s_path	**paths;
	char			*id;
	int				ant;
	size_t			links;
	size_t			size;
	size_t			occupied;
	size_t			start;
	size_t			end;
}					t_path;

typedef struct		s_paths
{
	t_path			**paths;
	int				ants;
	size_t			size;
}					t_paths;

typedef struct		s_matrix
{
	t_paths			*paths;
	char			***matrix;
	size_t			size;
}					t_matrix;

/*
**	init_paths() will create a t_paths struct holding an array
**	of t_path variables which hold empty arrays the size of
**	the total amount of rooms. The function returns a pointer to
**	the t_paths struct.
*/

typedef struct		s_doubly
{
	t_path			*path;
	t_path			*parent;
	struct s_doubly	*next;
}					t_doubly;

t_paths				*init_paths(size_t paths, t_paths **graphs);
t_matrix			*init_matrix(size_t path, t_matrix **matrix,
	t_paths **paths);
void				insert_room(t_paths *paths, size_t index, char *id);
void				create_link(t_paths **paths, char *from, char *to);
void				remove_path(t_paths **paths);

#endif
