/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils3.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 11:09:17 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/09/05 11:09:49 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

void			print_lines(t_list **lines)
{
	t_list		*ptr;

	ptr = *lines;
	while (ptr)
	{
		if (ptr->content)
			ft_putendl((char*)ptr->content);
		ptr = ptr->next;
	}
	ft_putchar('\n');
}

size_t			start_point(t_paths **paths)
{
	size_t		i;

	i = 0;
	while (i < (*paths)->size)
	{
		if ((*paths)->paths[i]->start)
			return (i);
		i++;
	}
	return (0);
}

int				in_stack(t_path *path, t_doubly **stacks)
{
	t_doubly	*ptr;

	ptr = *stacks;
	while (ptr)
	{
		if (ptr->path == path)
			return (1);
		ptr = ptr->next;
	}
	return (0);
}

void			add_to_open(t_doubly **open)
{
	t_doubly	*ptr;
	t_doubly	*ptr_sub;
	size_t		x;

	ptr = *open;
	while (ptr)
	{
		if (ptr->path->occupied == 0)
		{
			x = 0;
			while (x < ptr->path->links)
			{
				ptr_sub = ptr;
				while (ptr_sub->next)
					ptr_sub = ptr_sub->next;
				if (no_match(ptr->path->paths[x], *open))
					ptr_sub->next = new_route(ptr->path->paths[x], ptr->path);
				x++;
			}
			ptr->path->occupied = 1;
		}
		ptr = ptr->next;
	}
}

void			print_list(t_doubly *open)
{
	t_doubly	*ptr;

	ptr = open;
	while (ptr)
	{
		ft_putsgreen("|");
		ft_putsblue(ptr->path->id);
		ft_putsgreen("| ");
		ft_putendl("");
		ptr = ptr->next;
	}
}
