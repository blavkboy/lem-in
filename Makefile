SRC = src/find_paths.c src/get_links.c src/get_rooms.c src/guidance.c src/main.c \
	src/parsers.c src/path_finder.c src/read_comprehend.c src/reader.c src/utils1.c \
	src/utils2.c src/utils3.c src/utils4.c paths/inform_path.c paths/init_paths.c \
	paths/remove_paths.c
OBJ = find_paths.o get_links.o get_rooms.o guidance.o main.o parsers.o path_finder.o \
	read_comprehend.o reader.o utils1.o utils2.o utils3.o utils4.o inform_path.o \
	init_paths.o remove_paths.o
INC = -I./include/ -I./paths -I./libft
NAME = lem-in

all: lib $(NAME)

$(NAME): $(OBJ)
	gcc -o $(NAME) $(OBJ) $(INC) -L./libft -lft -Wall -Wextra -Werror

lib:
	make -C libft/

$(OBJ): $(SRC)
	gcc -c $(SRC) $(INC) -Wall -Wextra -Werror

clean:
	rm -rf $(OBJ)

fclean: clean
	make fclean -C libft/
	rm -rf $(NAME)

re: fclean all
