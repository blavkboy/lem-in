/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils4.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 11:10:10 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/09/05 11:42:24 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

void			link_items(t_paths **paths, t_list *ptr, t_list **lines)
{
	char		**links;
	size_t		i;

	i = 0;
	links = ft_strsplit((char*)ptr->content, '-');
	if (!ft_strequ(links[0], links[1]))
		create_link(paths, links[0], links[1]);
	else
	{
		remove_path(paths);
		report_error_lines("Error: room links to self.\n", lines, 1);
	}
	while (links[i])
	{
		free(links[i]);
		i++;
	}
	free(links[i]);
	free(links);
}

int				outcome_rooms(int n, t_list **lines)
{
	if (n && exists_endif(lines, is_end) && exists_endif(lines, is_start))
		return (1);
	return (0);
}

void			check_duplicates(t_list **head, t_paths **paths)
{
	t_list		*ptr;
	t_list		*ptr2;

	ptr = (*head)->next;
	while (ptr)
	{
		ptr2 = (*head);
		while (ptr2 != ptr)
		{
			if (ft_strequ((char*)ptr->content, (char*)ptr2->content) &&
				if_comment(ptr->content) == 0)
			{
				report_error_lines("Error: Duplicate instructions.\n", head, 0);
				remove_path(paths);
				exit(1);
			}
			ptr2 = ptr2->next;
		}
		ptr = ptr->next;
	}
}

int				exists_endif(t_list **head, int (*f)(char *))
{
	t_list		*ptr;

	ptr = (*head);
	while (ptr)
	{
		if (f((char*)ptr->content))
			return (1);
		ptr = ptr->next;
	}
	return (0);
}
