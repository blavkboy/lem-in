/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   remove_paths.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:37:19 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/02 10:37:21 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <paths.h>

void			remove_path(t_paths **paths)
{
	size_t		i;

	i = 0;
	while (i < (*paths)->size)
	{
		if ((*paths)->paths[i]->id != NULL)
			ft_strdel(&(*paths)->paths[i]->id);
		free((*paths)->paths[i]->paths);
		free((*paths)->paths[i]);
		i++;
	}
	free((*paths)->paths);
	free((*paths));
	*paths = NULL;
}
