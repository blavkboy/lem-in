/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   find_paths.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 11:02:48 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/09/05 11:02:50 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

t_doubly		*new_route(t_path *path, t_path *parent)
{
	t_doubly	*ptr;

	ptr = ft_memalloc(sizeof(t_doubly));
	ptr->next = NULL;
	ptr->path = path;
	ptr->parent = parent;
	return (ptr);
}

static void		init_route(t_doubly **route, size_t size, t_paths **paths)
{
	size_t		i;

	i = 0;
	while (i < size)
	{
		if ((*paths)->paths[i]->start)
		{
			*route = new_route((*paths)->paths[i], NULL);
		}
		i++;
	}
}

int				no_match(t_path *path, t_doubly *open)
{
	while (open)
	{
		if (open->path == path)
			return (0);
		open = open->next;
	}
	return (1);
}

int				contains_end(t_doubly *open)
{
	while (open)
	{
		if (open->path->end)
			return (1);
		open = open->next;
	}
	return (0);
}

void			find_paths(t_paths **paths, t_doubly **routes, t_list **lines)
{
	t_doubly	*open;

	init_route(&open, (*paths)->size, paths);
	add_to_open(&open);
	zero_paths(paths);
	if (contains_end(open))
	{
		print_lines(lines);
		path_maker(&open, paths);
	}
	*routes = open;
}
