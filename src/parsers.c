/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parsers.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/24 11:41:38 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/08/24 13:17:28 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

/*
**	The function below will return the number of ants the program has to
**	get through the rooms through the connections.
*/

int				num_ants(char *line)
{
	size_t		i;

	i = 0;
	while (line[i])
	{
		if (!ft_isdigit(line[i]))
			return (0);
		i++;
	}
	return (ft_atoll(line) > MAX_INT || ft_atoll(line) <= 0 ? 0 :
			(int)ft_atoll(line));
}

/*
**	if_comment() takes the line and confirms if the first two
**	characters are for a comment or not. If they are a comment the function
**	returns 1 else it returns 0.
*/

int				if_comment(char *line)
{
	if (line[0] == '#' && (!ft_strequ(line, "##start")
		&& !ft_strequ(line, "##end")))
		return (1);
	return (0);
}

/*
**	if_link() confirms if the line in question is a link.
**	If the line describes a link then it will return 1 else 0.
*/

int				if_link(char *line)
{
	size_t		i;
	int			checks[3];

	ft_bzero((void*)checks, sizeof(checks));
	i = 0;
	if (line[i] && ft_isprint(line[i]) && line[i] != ' ')
		checks[0] = 1;
	while (line[i] && ft_isprint(line[i]) && (line[i] != '-' && line[i] != ' '))
		i++;
	if (line[i] == '-')
	{
		checks[1] = 1;
		i++;
	}
	if (line[i] && ft_isprint(line[i]) && line[i] != ' ')
		checks[2] = 1;
	while (line[i] && ft_isprint(line[i]) && (line[i] != '-' && line[i] != ' '))
		i++;
	if (!line[i] && checks[0] == 1 && checks[1] == 1 && checks[2] == 1)
		return (1);
	return (0);
}

int				if_room(char *line)
{
	size_t		y;
	size_t		x;
	size_t		counter;

	counter = 0;
	y = 0;
	if (line[0] == 'L')
		return (0);
	while (line[y] != ' ' && line[y] && line[y] != '-')
		y++;
	if (line[y] == '-')
		return (0);
	y = (line[y] == ' ' && y > 0 ? y + 1 : y);
	if (y == 0 || line[y] == '\0')
		return (0);
	x = y;
	while (line[x] && counter < 2)
	{
		if (!ft_isdigit(line[x]) && line[x] != ' ')
			return (0);
		counter = line[x] == ' ' ? counter + 1 : counter;
		x++;
	}
	return (counter == 1 ? 1 : 0);
}
