/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init_paths.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:37:16 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/05 11:12:30 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <paths.h>

static void		clean_paths(t_path **ptr, size_t size)
{
	size_t		i;

	i = 0;
	while (i < size)
	{
		ft_memdel((void**)&ptr[i]);
		i++;
	}
}

static t_path	**make_paths(size_t paths)
{
	size_t		i;
	t_path		**ptr;

	if ((ptr = ft_memalloc(sizeof(t_path*) * paths)) == NULL)
		return (NULL);
	i = 0;
	while (i < paths)
	{
		if ((ptr[i] = ft_memalloc(sizeof(t_path))) == NULL)
		{
			clean_paths(ptr, i);
			ft_memdel((void**)&ptr);
			return (NULL);
		}
		ptr[i]->paths = ft_memalloc(sizeof(t_path*) * paths);
		ptr[i]->ant = 0;
		ptr[i]->start = 0;
		ptr[i]->end = 0;
		ptr[i]->occupied = 0;
		ptr[i]->size = paths;
		ptr[i]->links = 0;
		ptr[i]->id = NULL;
		i++;
	}
	return (ptr);
}

t_paths			*init_paths(size_t paths, t_paths **graphs)
{
	if ((*graphs = ft_memalloc(sizeof(t_paths))) == NULL)
		return (NULL);
	(*graphs)->paths = make_paths(paths);
	(*graphs)->size = paths;
	(*graphs)->ants = 0;
	return (*graphs);
}

t_matrix		*init_matrix(size_t path, t_matrix **matrix, t_paths **paths)
{
	size_t		i;

	i = 0;
	*matrix = ft_memalloc(sizeof(t_matrix));
	(*matrix)->size = path;
	(*matrix)->paths = *paths;
	(*matrix)->matrix = ft_memalloc(sizeof(char**) * (path));
	while (i < (*matrix)->size)
	{
		(*matrix)->matrix[i] = ft_memalloc(sizeof(char*) * (path));
		i++;
	}
	return (*matrix);
}
