/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rooms.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:05:27 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/05 11:06:38 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

static char	*ft_id(char *room)
{
	return (ft_strsub(room, 0, first_space(room)));
}

static int	compare_rooms(char *room_line, char *room)
{
	char	*room_2;

	room_2 = ft_strsub(room_line, 0, first_space(room_line));
	if (ft_strequ(room, room_2))
	{
		free(room_2);
		return (1);
	}
	free(room_2);
	return (0);
}

static void	check_room_dup(t_list *ptr, t_list **lines)
{
	char	*name;
	t_list	*itr_ptr;

	name = ft_id((char*)ptr->content);
	itr_ptr = (*lines)->next;
	while (itr_ptr)
	{
		if (if_room((char*)itr_ptr->content))
		{
			if (compare_rooms((char*)itr_ptr->content, name) &&
				itr_ptr->content != ptr->content)
			{
				report_error_lines("Error: Duplicate rooms not allowed.\n",
					lines, 0);
				ft_strdel(&name);
				exit(1);
			}
		}
		itr_ptr = itr_ptr->next;
	}
	ft_strdel(&name);
}

void		get_rooms(t_list **lines, t_list *ptr, size_t count)
{
	while (1)
	{
		if (if_link((char*)ptr->content) && count <= 1)
			report_error_lines("Error: Bad link.\n", lines, 1);
		else if (if_link((char*)ptr->content))
			return ;
		else if (outcome_rooms(if_link((char*)ptr->content), lines))
			count_starts(ptr, lines);
		else if (is_end((char*)ptr->content))
			count_ends(ptr, lines);
		else if (if_room((char*)ptr->content))
		{
			if (count)
				check_room_dup(ptr, lines);
			count++;
		}
		else if (!num_ants((char*)ptr->content) &&
				!if_comment((char*)ptr->content)
			&& (!is_end((char*)ptr->content) && !is_start((char*)ptr->content)))
			report_error_lines("Error: Incorrect data entry.\n", lines, 1);
		ptr->next = ft_lstnew(NULL, 0);
		ptr = ptr->next;
		if (!get_next_line(0, (char**)(&ptr->content)))
			report_error_lines("Error: insufficient data.\n", lines, 1);
	}
}
