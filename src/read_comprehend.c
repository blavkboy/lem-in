/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   read_comprehend.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:05:46 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/02 10:05:48 by gladi8r          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

void			test_paths(t_paths **paths)
{
	size_t		i;
	size_t		x;

	i = 0;
	ft_putscyan("Num ants: ");
	ft_putnbr((*paths)->ants);
	ft_putchar('\n');
	while (i < (*paths)->size)
	{
		x = 0;
		ft_putsyellow("Room: ");
		ft_putendl((*paths)->paths[i]->id);
		while (x < (*paths)->paths[i]->links)
		{
			ft_putscyan("links ");
			ft_putendl((*paths)->paths[i]->paths[x]->id);
			x++;
		}
		i++;
	}
}

void			start_parse(t_list **lines, t_paths **paths)
{
	t_list		*ptr;
	t_doubly	*routes;
	size_t		count;

	count = 0;
	ptr = *lines;
	routes = NULL;
	while (ptr->next)
		ptr = ptr->next;
	get_rooms(lines, ptr, count);
	if (!exists_endif(lines, is_end) || !exists_endif(lines, is_start))
		report_error_lines("Error: Missing start or end.\n", lines, 1);
	get_links(paths, lines);
	find_start_end(lines, paths);
	find_paths(paths, &routes, lines);
	free_lines(lines);
	remove_path(paths);
	exit(1);
}
