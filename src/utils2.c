/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils2.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 11:09:04 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/09/05 11:09:06 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

static void	find_point(t_list *start, t_paths **paths, char key)
{
	size_t	i;
	t_list	*ptr;

	i = 0;
	ptr = start;
	while (!if_room((char*)ptr->content) && ptr->next)
		ptr = ptr->next;
	while (i < (*paths)->size)
	{
		if (ft_strnequ((char*)ptr->content, (*paths)->paths[i]->id,
			first_space((char*)ptr->content)) && key == 's')
			(*paths)->paths[i]->start = 1;
		if (ft_strnequ((char*)ptr->content, (*paths)->paths[i]->id,
			first_space((char*)ptr->content)) && key == 'e')
			(*paths)->paths[i]->end = 1;
		i++;
	}
}

void		find_start_end(t_list **lines, t_paths **paths)
{
	size_t	i;
	t_list	*ptr;

	ptr = *lines;
	while (ptr)
	{
		if (is_start((char*)ptr->content))
			find_point(ptr, paths, 's');
		if (is_end((char*)ptr->content))
			find_point(ptr, paths, 'e');
		ptr = ptr->next;
	}
	i = 0;
	while (i < (*paths)->size)
	{
		if ((*paths)->paths[i]->start && (*paths)->paths[i]->end)
		{
			remove_path(paths);
			report_error_lines("Error: start and end is the same.\n", lines, 1);
		}
		i++;
	}
}

void		zero_paths(t_paths **paths)
{
	size_t	i;

	i = 0;
	while ((*paths)->size > i)
	{
		(*paths)->paths[i]->occupied = 1;
		i++;
	}
}
