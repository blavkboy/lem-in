/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   path_finder.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 11:07:02 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/09/05 11:07:03 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

static	void	prepend_list(t_doubly **path, t_doubly *ptr)
{
	t_doubly	*ptr1;

	ptr1 = new_route(ptr->path, NULL);
	ptr1->next = *path;
	*path = ptr1;
}

static void		free_stacks(t_doubly **stacks)
{
	t_doubly	*ptr;

	while ((*stacks)->next != NULL)
	{
		ptr = *stacks;
		*stacks = (*stacks)->next;
		free(ptr);
	}
	free((*stacks));
}

void			path_finder(t_doubly **stacks)
{
	t_doubly	*ptr1;
	t_doubly	*ptr2;
	t_path		*par;

	ptr1 = *stacks;
	while (ptr1->next && !ptr1->path->end)
		ptr1 = ptr1->next;
	ptr1->path->occupied = 0;
	ptr2 = new_route(ptr1->path, NULL);
	par = ptr1->parent;
	while (!in_stack((*stacks)->path, &ptr2))
	{
		ptr1 = *stacks;
		while (ptr1->path != par)
			ptr1 = ptr1->next;
		par = ptr1->parent;
		prepend_list(&ptr2, ptr1);
	}
	free_stacks(stacks);
	*stacks = ptr2;
}

void			path_maker(t_doubly **stacks, t_paths **paths)
{
	path_finder(stacks);
	guide_ants(stacks, paths);
	free_stacks(stacks);
}
