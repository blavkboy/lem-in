/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   inform_path.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:37:12 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/05 11:11:34 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <paths.h>

void		insert_room(t_paths *paths, size_t index, char *id)
{
	paths->paths[index]->id = id;
}

/*
**	static int	in_path(t_paths **paths, size_t from, size_t to)
**	{
**		size_t	i;
**
**		i = 0;
**		while (i < (*paths)->paths[to]->links)
**		{
**			if ((*paths)->paths[to]->paths[i] == (*paths)->paths[from])
**				return (1);
**			i++;
**		}
**		return (0);
**	}
*/

static void	insert_link(t_paths **paths, int from, int to)
{
	if ((*paths)->paths[from]->links + 1 < (*paths)->paths[from]->size)
	{
		(*paths)->paths[from]->paths[(*paths)->paths[from]->links] =
			(*paths)->paths[to];
		(*paths)->paths[from]->links++;
	}
}

void		create_link(t_paths **paths, char *from, char *to)
{
	size_t	y;
	size_t	x;

	y = 0;
	x = 0;
	while (y < (*paths)->size && !ft_strequ((*paths)->paths[y]->id, to))
		y++;
	while (x < (*paths)->size && !ft_strequ((*paths)->paths[x]->id, from))
		x++;
	if (x == (*paths)->size || y == (*paths)->size)
	{
		ft_putsred_fd(
				"Error: Bad Link.\n", 2);
		remove_path(paths);
		*paths = NULL;
		return ;
	}
	insert_link(paths, y, x);
	insert_link(paths, x, y);
}
