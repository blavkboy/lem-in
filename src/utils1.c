/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils1.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:06:08 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/05 11:08:44 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

int				first_space(char *str)
{
	size_t	i;

	i = 0;
	while (str[i] && str[i] != ' ')
		i++;
	return (i);
}

void			report_error_lines(char *str, t_list **lines, int kill)
{
	free_lines(lines);
	ft_putsred_fd(str, 2);
	if (kill == 1)
		exit(1);
}

size_t			num_rooms(t_list **lines)
{
	size_t		count;
	t_list		*ptr;

	count = 0;
	ptr = *lines;
	while (ptr)
	{
		if (if_room((char*)ptr->content))
			count++;
		ptr = ptr->next;
	}
	return (count);
}

void			count_starts(t_list *ptr, t_list **lines)
{
	t_list		*test;

	test = *lines;
	while (test != ptr && test)
	{
		if (ft_strequ(test->content, ptr->content))
			report_error_lines("Error: Duplicate starts.\n", lines, 1);
		test = test->next;
	}
}

void			count_ends(t_list *ptr, t_list **lines)
{
	t_list		*test;

	test = *lines;
	while (test != ptr && test)
	{
		if (ft_strequ(test->content, ptr->content))
			report_error_lines("Error: Duplicate ends.\n", lines, 1);
		test = test->next;
	}
}
