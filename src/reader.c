/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reader.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:05:56 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/05 11:07:33 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

int					is_start(char *line)
{
	if (ft_strequ(line, "##start"))
		return (1);
	return (0);
}

int					is_end(char *line)
{
	if (ft_strequ(line, "##end"))
		return (1);
	return (0);
}

static int			ant_num(char *line)
{
	while (ft_isdigit(*line))
		line++;
	return (*line == '\0' ? 1 : 0);
}

void				free_lines(t_list **head)
{
	t_list			*ptr;

	while ((*head)->next != NULL)
	{
		ptr = *head;
		*head = (*head)->next;
		free(ptr->content);
		free(ptr);
	}
	free((*head)->content);
	free((*head));
}

void				read_lines(t_paths **paths)
{
	t_list			*ptr;
	t_list			*lines;

	lines = ft_lstnew(NULL, 0);
	ptr = lines;
	while (get_next_line(0, (char**)&(ptr->content)))
	{
		if (if_comment((char*)ptr->content))
		{
			ptr->next = ft_lstnew(NULL, 0);
			ptr = ptr->next;
			continue ;
		}
		if (ant_num((char*)ptr->content))
			start_parse(&lines, paths);
		else
		{
			ft_putsred_fd("Error: Invalid input.\n", 2);
			free_lines(&lines);
			exit(1);
		}
	}
}
