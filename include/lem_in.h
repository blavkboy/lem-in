/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lem_in.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/24 11:42:13 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/09/05 11:39:04 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LEM_IN_H
# define LEM_IN_H

# include <libft.h>
# include <paths.h>

void		count_starts(t_list *ptr, t_list **lines);
void		count_ends(t_list *ptr, t_list **lines);
void		check_duplicates(t_list **head, t_paths **paths);
void		read_lines(t_paths **paths);
int			num_ants(char *line);
int			if_room(char *line);
int			if_link(char *line);
int			if_comment(char *line);
int			is_start(char *line);
int			is_end(char *line);
void		start_parse(t_list **lines, t_paths **paths);
void		free_lines(t_list **head);
int			first_space(char *str);
void		report_error_lines(char *str, t_list **lines, int kill);
void		get_rooms(t_list **lines, t_list *ptr, size_t count);
int			in_stack(t_path *path, t_doubly **stacks);
int			no_match(t_path *path, t_doubly *open);
void		link_items(t_paths **paths, t_list *ptr, t_list **lines);
size_t		num_rooms(t_list **lines);
void		get_links(t_paths **paths, t_list **lines);
int			exists_endif(t_list **head, int (*f)(char *));
int			outcome_rooms(int n, t_list **lines);
void		find_start_end(t_list **lines, t_paths **paths);
void		zero_paths(t_paths **paths);
t_doubly	*new_route(t_path *path, t_path *parent);
void		find_paths(t_paths **paths, t_doubly **routes, t_list **lines);
void		add_to_open(t_doubly **open);
void		path_maker(t_doubly **stacks, t_paths **paths);
void		guide_ants(t_doubly **stack, t_paths **paths);
void		print_lines(t_list **lines);
void		test_paths(t_paths **paths);

#endif
