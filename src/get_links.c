/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_links.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gladi8r <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 10:04:46 by gladi8r           #+#    #+#             */
/*   Updated: 2018/09/05 11:41:13 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

static size_t	room_count(t_list *ptr)
{
	size_t		count;

	count = 0;
	while (ptr)
	{
		if (if_room((char*)ptr->content))
			count++;
		ptr = ptr->next;
	}
	return (count);
}

static void		set_ants(t_list **lines, t_paths **paths)
{
	t_list		*ptr;

	ptr = *lines;
	while (ptr)
	{
		if (num_ants((char*)ptr->content))
			(*paths)->ants = num_ants((char*)ptr->content);
		ptr = ptr->next;
	}
}

static void		setup_rooms(t_list **lines, t_paths **paths, size_t rooms)
{
	size_t		i;
	t_list		*ptr;

	i = 0;
	ptr = *lines;
	set_ants(lines, paths);
	while (ptr && i < rooms)
	{
		if (if_room((char*)ptr->content))
		{
			insert_room(*paths, i, ft_strsub((char*)ptr->content,
				0, first_space((char*)ptr->content)));
			i++;
		}
		ptr = ptr->next;
	}
}

void			connect_rooms(t_list **lines, t_paths **paths)
{
	t_list		*ptr;

	ptr = (*lines)->next;
	while (ptr->next)
		ptr = ptr->next;
	if (if_link((char*)ptr->content))
		link_items(paths, ptr, lines);
	ptr->next = ft_lstnew(NULL, 0);
	ptr = ptr->next;
	while (get_next_line(0, (char**)(&ptr->content)))
	{
		if (if_link((char*)ptr->content))
			link_items(paths, ptr, lines);
		else if (!if_comment((char*)ptr->content))
		{
			remove_path(paths);
			report_error_lines("Error: Invalid link.\n", lines, 1);
			exit(1);
		}
		if (*paths == NULL)
			report_error_lines("Error: Invalid link.\n", lines, 1);
		check_duplicates(lines, paths);
		ptr->next = ft_lstnew(NULL, 0);
		ptr = ptr->next;
	}
}

void			get_links(t_paths **paths, t_list **lines)
{
	size_t		rooms;
	t_list		*ptr;

	ptr = *lines;
	rooms = room_count(ptr);
	init_paths(rooms, paths);
	setup_rooms(lines, paths, rooms);
	connect_rooms(lines, paths);
}
