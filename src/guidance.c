/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   guidance.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: gmohlamo <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/05 11:04:29 by gmohlamo          #+#    #+#             */
/*   Updated: 2018/09/05 11:04:30 by gmohlamo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <lem_in.h>

/*
**	The idea here is to guide our ants though the path
**	by keeping track of the ants in the game by using an array
**	for the ants in transit.
*/

static size_t	room_count(t_doubly **stack)
{
	size_t		i;
	t_doubly	*ptr;

	i = 0;
	ptr = *stack;
	while (ptr)
	{
		i++;
		ptr = ptr->next;
	}
	return (i);
}

static void		init_route(t_path ***path, t_doubly **stack, size_t rooms)
{
	t_doubly	*ptr;
	size_t		i;

	i = 0;
	ptr = *stack;
	*path = ft_memalloc(sizeof(t_path*) * rooms);
	while (ptr)
	{
		ptr->path->occupied = 0;
		(*path)[i] = ptr->path;
		i++;
		ptr = ptr->next;
	}
	(*path)[0]->occupied = rooms;
}

void			guide_ants(t_doubly **stack, t_paths **paths)
{
	int			next_ant;
	t_path		**path;
	size_t		rooms;
	size_t		i;

	rooms = room_count(stack);
	next_ant = 1;
	init_route(&path, stack, rooms);
	while (next_ant <= (*paths)->ants)
	{
		i = 0;
		while (i < rooms)
		{
			ft_putstr("L");
			ft_putnbr(next_ant);
			ft_putstr("-");
			ft_putendl(path[i]->id);
			i++;
		}
		next_ant++;
	}
	free(path);
}
